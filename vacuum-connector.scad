outer = 46;
inner = 40;
height = 25;

difference() {
    cylinder(height, d = outer, center = false);
    translate([ (outer - inner) / 3, 0]) cylinder(height, d = inner, center = false);
}


