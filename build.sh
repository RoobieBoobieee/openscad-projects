#!/bin/bash

FILES=$1
if [ "$FILES" = "" ]; then
    FILES="*.scad"
fi

mkdir -p output

for FILENAME in $FILES; do
    # Get name without extension
    NAME=$(echo "$FILENAME" | sed 's/\.scad$//')

    echo "Building $NAME"

    # Make output folder
    mkdir -p output/$NAME

    # Generate STL
    echo "- Generating STL"
    openscad -o output/$NAME/$NAME.stl $NAME.scad

    # Generate PNG
    echo "- Generating PNG"
    openscad -o output/$NAME/$NAME.png $NAME.scad
done
